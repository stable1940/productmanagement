import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  ViewChild,
  Input
} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductEditComponent } from '../product-edit/product-edit.component';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: any[];
  product: any;
  form: FormGroup;
  display: string;

  @ViewChild('productEdit') productEdit: ProductEditComponent;
  @Output() messageEvent = new EventEmitter<string>();
  constructor(private httpClient: HttpClient, private fb: FormBuilder) {
    this.form = this.fb.group({
      id: '',
      name: '',
      detail: '',
      price: ''
    });
  }

  ngOnInit() {
    this.httpClient
      .get('http://localhost:8555/getAllProduct')
      .subscribe(result => {
        this.products = result as any[];
        this.product = {
          id: '',
          name: '',
          detail: '',
          price: ''
        };
      });
  }

  loadProduct() {
    this.products = [];
    this.httpClient
      .get('http://localhost:8555/getAllProduct')
      .subscribe(result => {
        this.products = result as any[];
        this.product = {
          id: '',
          name: '',
          detail: '',
          price: ''
        };
      });
  }

  clickMethod(id: number, name: string) {
    if (confirm('Are you sure to delete ' + name)) {
      console.log('Implement delete functionality here');
      this.httpClient
        .get('http://localhost:8555/deleteProductById/' + id)
        .subscribe(result => {
          this.form.reset();
          alert('Delete Product Success !');
          this.loadProduct();
        });
    }
  }
  editProduct(product: any[]) {
    this.product.id = product['id'];
    this.product.name = product['name'];
    this.product.detail = product['detail'];
    this.product.price = product['price'];
    this.display = 'block';
  }
  onCloseHandled() {
    this.display = 'none';
  }
  saveProduct() {
    let id: number = this.product.id;
    let name: string;
    let detail: string;
    let price: number;

    if (this.form.value.name === '') {
      name = this.product.name;
    } else {
      name = this.form.value.name;
    }
    if (this.form.value.detail === '') {
      detail = this.product.detail;
    } else {
      detail = this.form.value.detail;
    }
    if (this.form.value.price === '') {
      price = this.product.price;
    } else {
      price = this.form.value.price;
    }

    this.httpClient
      .get(
        'http://localhost:8555/editProduct/' +
          id +
          '/' +
          name +
          '/' +
          detail +
          '/' +
          price
      )
      .subscribe(result => {
        this.form.reset();
        // alert('Add Product ' + this.form.value.name + ' Success !');
      });
    alert('Edit Product Success !');
    this.onCloseHandled();
    this.loadProduct();
  }
}
