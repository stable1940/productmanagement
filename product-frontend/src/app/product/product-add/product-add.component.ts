import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-add,parent',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {
  form: FormGroup;
  @Output() messageEvent = new EventEmitter<string>();
  constructor(private httpClient: HttpClient, private fb: FormBuilder) {
    this.form = this.fb.group({
      name: '',
      detail: '',
      price: ''
    });
  }

  ngOnInit() {}
  addProduct() {
    this.httpClient
      .get(
        'http://localhost:8555/createProduct/' +
          this.form.value.name +
          '/' +
          this.form.value.detail +
          '/' +
          this.form.value.price
      )
      .subscribe(result => {
        this.form.reset();
        // alert('Add Product ' + this.form.value.name + ' Success !');
      });
    this.messageEvent.emit(this.form.value.name);
  }
}
