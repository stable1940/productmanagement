import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductAddComponent } from '../product-add/product-add.component';
import { ProductListComponent } from '../product-list/product-list.component';
import { ProductEditComponent } from '../product-edit/product-edit.component';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @ViewChild('productAdd') productAdd: ProductAddComponent;
  @ViewChild('productList') productList: ProductListComponent;
  @ViewChild('productEdit') productEdit: ProductEditComponent;
  message: string;
  constructor() {}
  ngOnInit() {}
  refreshProduct(message: String) {
    alert('Add Product Success !');
    this.productList.loadProduct();
  }
  editProduct(id: number) {
    alert('Add Product Success !');
    this.productEdit.editProduct(id);
  }
}
