import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  @Input()
  name: string;
  @Input()
  price: number;
  @Input()
  detail: string;
  form: FormGroup;
  constructor(private httpClient: HttpClient, private fb: FormBuilder) {
    this.form = this.fb.group({
      id: '',
      name: '',
      detail: '',
      price: ''
    });
  }

  ngOnInit() {}
  editProduct(id: number) {
    this.httpClient
      .get('http://localhost:8555/getProductById' + id)
      .subscribe(result => {
        this.form.value.id = result['id'];
        this.form.value.name = result['name'];
        this.form.value.detail = result['detail'];
        this.form.value.price = result['price'];
      });
  }
}
