export class Product {
  id: number;
  name: String;
  detail: String;
  price: number;
}
