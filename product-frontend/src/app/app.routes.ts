import { Routes } from '@angular/router';

import { ProductComponent } from './product/product/product.component';
import { BranchComponent } from './branch/branch/branch.component';

export const rootRouterConfig: Routes = [
  { path: '', redirectTo: 'product', pathMatch: 'full' },
  { path: 'product', component: ProductComponent },
  { path: 'branch', component: BranchComponent }
];
