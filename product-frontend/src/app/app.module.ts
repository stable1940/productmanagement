import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ProductComponent } from './product/product/product.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';
import { ProductAddComponent } from './product/product-add/product-add.component';
import { rootRouterConfig } from './app.routes';
import { BranchComponent } from './branch/branch/branch.component';
import { BranchAddComponent } from './branch/branch-add/branch-add.component';
import { BranchEditComponent } from './branch/branch-edit/branch-edit.component';
import { BranchListComponent } from './branch/branch-list/branch-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchbarComponent,
    ProductComponent,
    ProductListComponent,
    ProductEditComponent,
    ProductAddComponent,
    BranchComponent,
    BranchAddComponent,
    BranchEditComponent,
    BranchListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: false })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
