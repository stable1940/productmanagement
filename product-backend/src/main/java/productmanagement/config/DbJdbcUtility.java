package productmanagement.config;

import productmanagement.entity.*;
//import productmanagement.entity.notEntity.*;
import com.zaxxer.hikari.HikariDataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class DbJdbcUtility {

    private JdbcTemplate jdbcTemplate;

    public DbJdbcUtility(HikariDataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

}
