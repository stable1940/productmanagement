package productmanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import productmanagement.entity.Branch;
import productmanagement.repository.BranchRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class BranchService {

    @Autowired
    private BranchRepository branchRepository;

    // findAll
    public List<Branch> getBranch() {
        String active = "active";
        return branchRepository.findByStatus(active);
    }

    public void createBranch(String name, String detail) {
        Branch branch = new Branch();
        branch.setName(name);
        branch.setDetail(detail);
        branch.setStatus("active");
        branchRepository.save(branch);
    }

    public void editBranch(int id, String name, String detail) {
        Branch branch = new Branch();
        branch.setId(id);
        branch.setName(name);
        branch.setDetail(detail);
        branch.setStatus("active");
        branchRepository.save(branch);
    }

    // findByName
    public List<Branch> getBranchByName(String name) {
        String active = "active";
        return branchRepository.findByNameAndStatus(name, active);
    }

    // findById
    public List<Branch> getBranchById(int id) {
        String active = "active";
        return branchRepository.findByIdAndStatus(id, active);
    }

    // Delete
    public ResponseEntity<Object> deleteBranchById(Integer id) {
        Optional<Branch> branchs = branchRepository.findById(id);
        Branch branch = branchs.get();
        branch.setStatus("deactivate");
        branchRepository.save(branch);
        return ResponseEntity.ok().build();
    }

}