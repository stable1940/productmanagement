package productmanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import productmanagement.entity.Product;
import productmanagement.repository.ProductRepository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    // findAll
    public List<Product> getProduct() {
        String active = "active";
        return productRepository.findByStatus(active);
    }

    public void createProduct(String name, String detail, BigDecimal price) {
        Product product = new Product();
        product.setName(name);
        product.setDetail(detail);
        product.setPrice(price);
        product.setStatus("active");
        productRepository.save(product);
    }

    public void editProduct(int id, String name, String detail, BigDecimal price) {
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        product.setDetail(detail);
        product.setPrice(price);
        product.setStatus("active");
        productRepository.save(product);
    }

    // findByName
    public List<Product> getProductByName(String name) {
        String active = "active";
        return productRepository.findByNameAndStatus(name, active);
    }

    // findById
    public List<Product> getProductById(int id) {
        String active = "active";
        return productRepository.findByIdAndStatus(id, active);
    }

    // Delete
    public ResponseEntity<Object> deleteProductById(Integer id) {
        Optional<Product> products = productRepository.findById(id);
        Product product = products.get();
        product.setStatus("deactivate");
        productRepository.save(product);
        // productRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

}