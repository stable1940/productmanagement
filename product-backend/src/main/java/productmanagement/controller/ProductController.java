package productmanagement.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import productmanagement.entity.Product;
import productmanagement.service.ProductService;

import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@CrossOrigin
public class ProductController {

    @Autowired
    ProductService productService;

    // @RequestMapping(value = { "/" }, method = RequestMethod.GET)
    // public String welcomePage(Model model) {
    // model.addAttribute("title", "Welcome");
    // model.addAttribute("message", "This is welcome page!");
    // return "welcomePage";
    // }

    @RequestMapping(value = "/getAllProduct", method = RequestMethod.GET)
    public List<Product> getProduct() {
        return productService.getProduct();
    }

    @RequestMapping(value = "/getProductByName/{name}", method = RequestMethod.GET)
    public List<Product> getProductByName(@PathVariable(value = "name") String name) {
        return productService.getProductByName(name);
    }

    @RequestMapping(value = "/getProductById/{id}", method = RequestMethod.GET)
    public List<Product> getProductById(@PathVariable(value = "id") int id) {
        return productService.getProductById(id);
    }

    @RequestMapping(value = "/createProduct/{name}/{detail}/{price}", method = RequestMethod.GET)
    public void createProduct(@PathVariable(value = "name") String name, @PathVariable(value = "detail") String detail,
            @PathVariable(value = "price") BigDecimal price) {
        productService.createProduct(name, detail, price);
    }

    @RequestMapping(value = "/editProduct/{id}/{name}/{detail}/{price}", method = RequestMethod.GET)
    public void editProduct(@PathVariable(value = "id") int id, @PathVariable(value = "name") String name,
            @PathVariable(value = "detail") String detail, @PathVariable(value = "price") BigDecimal price) {
        productService.editProduct(id, name, detail, price);
    }

    @RequestMapping(value = "/deleteProductById/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> deleteProductById(@PathVariable(value = "id") Integer id) {
        return productService.deleteProductById(id);
    }
}
