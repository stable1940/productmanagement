package productmanagement.controller;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import productmanagement.entity.Branch;
import productmanagement.service.BranchService;

import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@CrossOrigin
public class BranchController {

    @Autowired
    BranchService branchService;

    // @RequestMapping(value = { "/" }, method = RequestMethod.GET)
    // public String welcomePage(Model model) {
    // model.addAttribute("title", "Welcome");
    // model.addAttribute("message", "This is welcome page!");
    // return "welcomePage";
    // }

    @RequestMapping(value = "/getAllBranch", method = RequestMethod.GET)
    public List<Branch> getBranch() {
        return branchService.getBranch();
    }

    @RequestMapping(value = "/getBranchByName/{name}", method = RequestMethod.GET)
    public List<Branch> getBranchByName(@PathVariable(value = "name") String name) {
        return branchService.getBranchByName(name);
    }

    @RequestMapping(value = "/getBranchById/{id}", method = RequestMethod.GET)
    public List<Branch> getBranchById(@PathVariable(value = "id") int id) {
        return branchService.getBranchById(id);
    }

    @RequestMapping(value = "/createBranch/{name}/{detail}", method = RequestMethod.GET)
    public void createBranch(@PathVariable(value = "name") String name, @PathVariable(value = "detail") String detail) {
        branchService.createBranch(name, detail);
    }

    @RequestMapping(value = "/editBranch/{id}/{name}/{detail}", method = RequestMethod.GET)
    public void editBranch(@PathVariable(value = "id") int id, @PathVariable(value = "name") String name,
            @PathVariable(value = "detail") String detail) {
        branchService.editBranch(id, name, detail);
    }

    @RequestMapping(value = "/deleteBranchById/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> deleteBranchById(@PathVariable(value = "id") Integer id) {
        return branchService.deleteBranchById(id);
    }
}
