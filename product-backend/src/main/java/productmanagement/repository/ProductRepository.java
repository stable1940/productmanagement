package productmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import productmanagement.entity.Product;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findByStatus(String status);

    List<Product> findByNameAndStatus(String name, String status);

    List<Product> findByIdAndStatus(int id, String status);

    void deleteById(Integer id);
}