package productmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import productmanagement.entity.Branch;

import java.util.List;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Integer> {
    List<Branch> findByStatus(String status);

    List<Branch> findByNameAndStatus(String name, String status);

    List<Branch> findByIdAndStatus(int id, String status);

    void deleteById(Integer id);
}